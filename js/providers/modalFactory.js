var app = angular.module('com.ekoiko.doeko');

app.factory('ModalFactory', function ($modal){
	var ModalUtils = function(){

	};

	ModalUtils.prototype = {
		/**
		  * Open a modal asking the user to confirm something
		  * @param modalTitle String
		  * @param modalMessage message
		  * @param onConfirm function - something to do of the user click
		  *			the confim button
		  * @param onCancel function - something to do if the user click
		  *			the cancel button [optional]
		*/
		confirm: function(modalTitle, modalMessage, onConfirm, onCancel){
			return openConfirmModal(
				modalTitle,
				modalMessage,
				onConfirm,
				onCancel,
				'./templates/modals/confirmModal.html'
			);
		},
		/**
		  * Same as confirm but the modal looks like a warning
		  * (orange header and exclamation mark)
		*/
		confirmWarning: function(modalTitle, modalMessage, onConfirm, onCancel){
			return openConfirmModal(
				modalTitle,
				modalMessage,
				onConfirm,
				onCancel,
				'./templates/modals/confirmModalWarning.html'
			);
		},
                /**
		  * Same as confirm but the modal looks like a warning
		  * (orange header and exclamation mark)
		*/
		confirmError: function(modalTitle, modalMessage, onConfirm, onCancel){
			return openConfirmModal(
				modalTitle,
				modalMessage,
				onConfirm,
				onCancel,
				'./templates/modals/confirmModalError.html'
			);
		},
		/**
		  * Open a modal to display an error message
		  * @param modalTitle String
		  * @param modalMessage message
		*/
		error: function(modalTitle, modalMessage){
			return openSimpleModal(modalTitle, modalMessage, './templates/modals/errorModal.html');
		},
                /**
		  * Open a modal to display a warning message
		  * @param modalTitle String
		  * @param modalMessage message
		*/
		warning: function(modalTitle, modalMessage){
			return openSimpleModal(modalTitle, modalMessage, './templates/modals/warningModal.html');
		},
		/**
		  * Open a modal to display some information
		  * @param modalTitle String
		  * @param modalMessage message
		*/
		info: function(modalTitle, modalMessage){
			return openSimpleModal(modalTitle, modalMessage, './templates/modals/infoModal.html');
		},
		/**
		  * Open a modal to ask the user to wait
		  * @param modalTitle String
		  * @param modalMessage message
		*/
		wait: function(modalTitle, modalMessage){
			return openSimpleModal(modalTitle, modalMessage, './templates/modals/waitModal.html');
		},
		/**
		  * Open a modal to tell the user something is successful
		  * @param modalTitle String
		  * @param modalMessage message
		*/
		success: function(modalTitle, modalMessage){
			return openSimpleModal(modalTitle, modalMessage, './templates/modals/successModal.html');
		}
	};

	function openSimpleModal(modalTitle, modalMessage, template){
		return $modal.open({
            templateUrl: template,
            controller: 'GenericModalController',
            backdrop: 'static',
            size: 'lg',
            resolve: {
                title: function () {
                    return modalTitle;
                },
                message: function () {
                	return modalMessage;
                }
            }
        });
	}

	function openConfirmModal(modalTitle, modalMessage, onConfirm, onCancel, template){
		return $modal.open({
            templateUrl: template,
            controller: 'ConfirmModalController',
            backdrop: 'static',
            size: 'lg',
            resolve: {
                title: function () {
                    return modalTitle;
                },
                message: function () {
                	return modalMessage;
                },
                onConfirm: function () {
                	return onConfirm;
                },
                onCancel: function () {
                	return onCancel;
                }
            }
        });
	}

	return new ModalUtils();
});

/**
  * A simple controller for simple modals
*/
app.controller('GenericModalController', function ($scope, title, message){
	$scope.title = title;
	$scope.message = message;
});

/**
  * A simple controller for simple confirm modals
*/
app.controller('ConfirmModalController', function ($scope, $modalInstance, title, message, onConfirm, onCancel){
	$scope.title = title;
	$scope.message = message;

	$scope.confirm = function (){
		$modalInstance.close();
		onConfirm();
	};

	$scope.cancel = function (){
		$modalInstance.close();
		if(onCancel != undefined){
			onCancel();
		}
	};
});