var doeko = angular.module('com.ekoiko.doeko');

doeko.factory('ProjectRestCaller', ['$http',
function ($http){
	
	var ProjectRestCaller = {
		createProject: function createProject (project){
			return $http.post('http://localhost:8080/projet', project);
		},
		getUserProjects: function getUserProjects(userId){
			return $http.get('http://localhost:8080/user/'+ userId +'/projets');
		},
		getProjectById: function getProjectById(projectId){
			return $http.get('http://localhost:8080/projet/'+ projectId);
		}
	};

	return ProjectRestCaller;
}]);