var doeko = angular.module('com.ekoiko.doeko');

doeko.controller('HomeController', ['$scope', '$location', 'AuthenticationFactory','ProjectRestCaller',
function ($scope, $location, AuthenticationFactory, ProjectRestCaller){
	$scope.getCurrentUser = AuthenticationFactory.getCurrentUser;
	if($scope.getCurrentUser().id != -1) {
		ProjectRestCaller.getUserProjects($scope.getCurrentUser().id)
				.success(function (projects) {
					if (projects.length > 0) {
						$location.path('/projet/' + projects[0].id);
					} else {
						$location.path('/new/project');
					}
				}).error(function (error) {
			console.log(error);
		});
	}
}]);