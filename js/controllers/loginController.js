var doeko = angular.module('com.ekoiko.doeko');

doeko.controller('LoginController', ['$scope', '$location', 'AuthenticationFactory', 'ModalFactory', "$sce", 'ProjectRestCaller',
function ($scope, $location, AuthenticationFactory, ModalFactory, $sce, ProjectRestCaller){
	$scope.action = 'signup';
	$scope.credentials = {
		firstName: null,
		lastName: null,
		login: null,
		password: null
	};

	$scope.setAction = function(action){
		$scope.action = action;
	};

	$scope.submit = function(){
		if($scope.action === 'signup'){
			AuthenticationFactory.signup(
				$scope.credentials.firstName,
				$scope.credentials.lastName,
				$scope.credentials.login,
				$scope.credentials.password
			);
		}
		if($scope.action === 'signin'){
			AuthenticationFactory.login(
				$scope.credentials.login,
				$scope.credentials.password)
			.then(function(){
				if($scope.getCurrentUser().id != -1){
					ProjectRestCaller.getUserProjects($scope.getCurrentUser().id)
							.success(function (projects){
								if(projects.length > 0){
									$location.path('/projet/'+projects[0].id);
								}else{
									$location.path('/new/project');
								}
							}).error(function (error){
						console.log(error);
					});

				}
			}, function(){
				ModalFactory.error(
					'Erreur',
                    $sce.trustAsHtml('Email inconnu ou mot de passe incorrect'));
			});
		}
	};
}]);