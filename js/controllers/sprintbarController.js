var doeko = angular.module('com.ekoiko.doeko');

doeko.controller('SprintbarController', ['$scope','$rootScope','$routeParams', 'AuthenticationFactory', 'SprintRestCaller', 'ProjectRestCaller',
function ($scope, $rootScope, $routeParams, AuthenticationFactory, SprintRestCaller, ProjectRestCaller){
    $scope.getCurrentUser = AuthenticationFactory.getCurrentUser;

    $scope.createSprint = function(){
        SprintRestCaller.createSprint($rootScope.currentProject.id)
            .success(function (sprint){
                $rootScope.currentProject.sprints.push(sprint);
            }).error(function (error){
            console.log(error);
        });
    };

    $scope.selectSprint = function(sprintId){
        SprintRestCaller.selectSprint($rootScope.currentProject.id, sprintId)
            .success(function (sprint){
                $rootScope.currentProject.selectedSprint = sprint;
                $('.selected-sprint').removeClass('selected-sprint');
                $('#sprint-'+sprintId).addClass('selected-sprint');
            }).error(function (error){
            console.log(error);
        });
    };

    $rootScope.selectLatestSprint = function selectLatestSprint(){
        //$scope.statuses[0].tickets = $rootScope.currentProject.backlog;

        var nbSprints = $rootScope.currentProject.sprints.length;
        if(nbSprints > 0){
            $scope.selectSprint($rootScope.currentProject.sprints[nbSprints-1].id);
        }
    };

}]);