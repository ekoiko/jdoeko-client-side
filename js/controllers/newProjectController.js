var doeko = angular.module('com.ekoiko.doeko');

doeko.controller('NewProjectController', ['$scope', '$location', '$uibModal', 'ProjectRestCaller', 'ModalFactory','AuthenticationFactory','SprintRestCaller',
function ($scope, $location, $modal, ProjectRestCaller, ModalFactory, AuthenticationFactory, SprintRestCaller){
	$scope.getCurrentUser = AuthenticationFactory.getCurrentUser;
    AuthenticationFactory.authorized();
	$scope.guests = [];

	if($scope.getCurrentUser().id != -1){
		$scope.guests.push({email: $scope.getCurrentUser().email});
	}
	/** RemoveUserFromGuests
	**  Retire l'utilisateur d'index index du tableau guests
	**  IN - index - l'index de l'utilisateur
	*/
	$scope.removeUserFromGuests = function(index){
		$scope.guests.splice(index,1);
	};

	/** OpenInviteModal
	**  Ouvre le modal permettant d'inviter des utilisateurs
	*/
	$scope.openInviteModal = function(){
		var inviteModalInstance = $modal.open({
			animation: true,
			templateUrl: 'templates/inviteModal.html',
			controller: 'InviteModalController'
		});	

		inviteModalInstance.result.then(function(returnValue){
			$scope.newGuests = returnValue;
			for(var i =0; i < $scope.newGuests.length; i++){
				$scope.guests.push({email: $scope.newGuests[i]});
			}
		});
	};

	/** CreateProject
	**  Créer le projet à la soumission du formulaire
	*/
	$scope.createProject = function(){
		var members = [];
		for(var i = 0; i < $scope.guests.length; i++){
			members.push($scope.guests[i].email);
		}
		ProjectRestCaller.createProject({
			name: $scope.projectTitle,
			members: members
		}).success(function (createdProject){
			$location.path('/projet/'+ createdProject.id);

		}).error(function (error){
			ModalFactory.error(
				'Erreur',
				'Une erreur est survenue, vérifiez votre saisie et réessayez.'
			);
		});
	}
}]);