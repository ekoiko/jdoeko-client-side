var doeko = angular.module('com.ekoiko.doeko');

doeko.factory('TicketRestCaller', ['$http',
    function ($http){

        var TicketRestCaller = {
            updateTicket: function updateTicket(projectId, ticketId, state){
                return $http.post('http://localhost:8080/projet/' +  projectId + '/ticket/'+ ticketId, {state: state});
            },
            createTicket: function createTicket(projectId,ticket){
                return $http.post('http://localhost:8080/projet/' +  projectId + '/ticket', ticket);
            }
        };

        return TicketRestCaller;
    }]);
