var doeko = angular.module('com.ekoiko.doeko');

doeko.controller('InviteModalController', ['$scope','$modalInstance',
function ($scope,$modal){
	$scope.newGuests = [];
	$scope.guests = [{
		firstname:"David",
		lastname:"Lapaluza",
		email:"David.Lapaluza@gmail.com"
	},
	{
		firstname:"Coco",
		lastname:"Legendary",
		email:"Coco.Legendary@gmail.com"
	},
	{
		firstname:"Bernard",
		lastname:"Sans-chaise",
		email:"Bernard.Sans-chaise@gmail.com"
	}];

	$scope.ok = function(){
		$modal.close($scope.newGuests);
	};
	$scope.cancel = function(){
		$modal.dismiss('cancel');
	};
	$scope.addNewGuest = function(){
		var newGuest = $scope.guest;
		$scope.newGuests.push(newGuest);
	};
	$scope.createNewProject = function(){

	};
}]);