var doeko = angular.module('com.ekoiko.doeko');

doeko.controller('NavbarController', ['$scope', '$location', 'AuthenticationFactory', 'ProjectRestCaller',
function ($scope, $location, AuthenticationFactory, ProjectRestCaller){
	$scope.isCollapsed = true;
	$scope.getCurrentUser = AuthenticationFactory.getCurrentUser;
    
    $scope.$on('$routeChangeSuccess', function (){
        $scope.isCollapsed = true;
    });

    $scope.getClass = function (path) {
        if(path === '/') {
            if($location.path() === '/') {
                return 'active';
            } else {
                return '';
            }
        }
        if ($location.path().substr(0, path.length) === path) {
            return 'active';
        } else {
            return '';
        }
    };

    $scope.getCurrentUserProjects = function (){
        init();
    };

    function init(){
        ProjectRestCaller.getUserProjects($scope.getCurrentUser().id)
        .success(function (projects){
            $scope.projects = projects;
        }).error(function (error){
            console.log(error);
        });
    }

    init();
}]);