var doeko = angular.module('com.ekoiko.doeko');

doeko.factory('SprintRestCaller', ['$http',
function ($http){

    var SprintRestCaller = {
        createSprint: function createSprint (projectId){
            return $http.post('http://localhost:8080/projet/' +  projectId + '/sprint', {});
        },
        selectSprint: function selectSprint (projectId, sprintId){
            return $http.get('http://localhost:8080/projet/' + projectId + '/sprint/'+ sprintId);
        }
    };

    return SprintRestCaller;
}]);
