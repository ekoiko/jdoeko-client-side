// on crée un module angular, en gros un module = un package
var doeko = angular.module('com.ekoiko.doeko', [
	'ui.bootstrap',
	'ngRoute',
	'ngDraggable'
]);

doeko.config(['$routeProvider',
function ($routeProvider){
	$routeProvider.
		when('/', {
			templateUrl: 'templates/home.html',
			controller: 'HomeController'
		}).
		when('/projet/:project_id?', {
			templateUrl: 'templates/dashboard.html',
			controller: 'DashboardController'
		}).
		when('/new/project', {
			templateUrl: 'templates/newProject.html',
			controller: 'NewProjectController'
		}).
		otherwise({
			redirectTo: '/'
		});
}]);
