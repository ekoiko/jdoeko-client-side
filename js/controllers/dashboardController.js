var doeko = angular.module('com.ekoiko.doeko');

doeko.controller('DashboardController', ['$scope','$rootScope','$routeParams','ProjectRestCaller','TicketRestCaller','AuthenticationFactory',
function ($scope, $rootScope, $routeParams, ProjectRestCaller, TicketRestCaller, AuthenticationFactory){

    AuthenticationFactory.authorized();
    $scope.statuses = [
        {
            name: 'Backlog',
            tickets: []
        },
        {
            name: 'A faire',
            tickets: [{
                id: 0,
                title: 'Fix security breach',
                description: 'Super important task to do'
            },{
                id: 11,
                title: 'Do stuff',
                description: 'Important task to do'
            },{
                id: 21,
                title: 'Refactoring',
                description: 'Not important at all. Do it if you\'re bored'
            },{
                id: 13,
                title: 'Great job John !',
                description: 'John broke stuff, now we have to fix it'
            }]
        },{
            name: 'En cours',
            tickets: [{
                id: 22,
                title: 'Do more stuff',
                description: 'There is always stuff to do'
            }]
        },{
            name: 'Terminé',
                tickets: [{
                id: 12,
                title: 'Database setup',
                description: 'Put everything in the same table and it will be fine'
            }]
        }
    ];


    $scope.moveTicket = function(dest, data, event){
        var srcStatus = $scope.statuses[data.statusIndex];
        var destStatus = $scope.statuses[dest];
        var ticket = srcStatus.tickets[data.ticketIndex];

        destStatus.tickets.push(ticket);
        srcStatus.tickets.splice(data.ticketIndex, 1);
    };

    $scope.createTicket = function(){
        var ticket = {
            title: 'Nouveau ticket',
            description: '...'
        };
        $scope.statuses[0].tickets.push(ticket);
        /*TicketRestCaller.createTicket($rootScope.currentProject.id, ticket)
            .success(function(ticket){
                //$rootScope.currentProject.backlog.push(ticket);
                $scope.statuses[0].tickets.push(ticket);
            }).error(function(error){
                console.log(error);
            });*/

    }

    function init(){
        if($routeParams.project_id){
            ProjectRestCaller.getProjectById($routeParams.project_id)
            .success(function (project){
                    $rootScope.currentProject = project;
                    console.log( $rootScope.currentProject.name);
                    $scope.statuses[0].tickets = $rootScope.currentProject.backlog;
                    $rootScope.selectLatestSprint();
            }).error(function (error){
                console.log(error);
            });
        }
    }

   init();
}]);
