var doeko = angular.module('com.ekoiko.doeko');

doeko.factory('AuthenticationFactory', ['$q', '$http', '$location',
function ($q, $http, $location){
	var logged = false;
	var currentUser = {
		isLogged: false, // true if a session is active
		id: -1, // id of the currently logged user
		name: '', // name of the currently logged user
		email: '' // email of the currently logged user
	};

	return {
		getCurrentUser: function(){
			return currentUser;
		},
        authorized: function authorized(){
            if(!logged){
                $location.path('/');
            }
        },
		/**
		 * Performs http request to sign the user in
		 * @return Promise
		*/
		login: function(login, password){
			var context = this;
			var deferred = $q.defer();

            $http.post('http://localhost:8080/login', {
                email: login,
                password: password
            }).success(function onLoginSuccess(user){
                logged = true;
                currentUser.isLogged = true;
                currentUser.id = user.id;
                currentUser.name = user.firstName +' '+ user.lastName;
                currentUser.email = user.email;
                deferred.resolve(user);
            }).error(function onLoginError(error){
                logged = false;
                currentUser.isLogged = false;
                currentUser.id = -1;
                currentUser.name = '';
                currentUser.email = '';
                deferred.reject(error);
            });

			return deferred.promise;
		},
		/**
		 * Performs http request to sign the user out
		 * @return Promise
		*/
		logout: function(){
			var context = this;
			var deferred = $q.defer();
			setTimeout(function() {
				logged = false;
				context.updateSession();
				deferred.resolve();
			}, 0);
			return deferred.promise;
		},
		/**
		 * Performs http request to update active session
		 * @return Promise
		*/
		updateSession: function(){
			var context = this;
			var deferred = $q.defer();
			setTimeout(function() {
				if(logged){
					deferred.resolve();
				} else {
					deferred.reject();
				}
			}, 0);
			return deferred.promise;
		},


		signup: function(firstname, lastname, email, password){
			var context = this;
			var data = {
				firstName: firstname,
				lastName: lastname,
				email: email,
				password: password
			};
			$http.post('http://localhost:8080/user',data)
				.success(function(response){
					console.log(response);
				});
		}
	};
}]);